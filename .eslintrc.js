// eslint-disable-next-line no-undef
module.exports = {
    'extends': 'eslint:recommended',
    'rules': {
        'no-console': 'error',
        'no-unused-vars': 'error',
        'no-undef': 'error',
        'no-var': 'error',
        'prefer-const': 'error',
        'arrow-spacing': 'error',
        'object-curly-spacing': 'error',
        'array-bracket-spacing': 'error',
        'prefer-arrow-callback': 'error',
        'arrow-parens': 'error',
        'quote-props': 'error',
        'quotes': ['error', 'single'],
        'comma-spacing': 'error',
        'no-trailing-spaces': 'error',
        'eol-last': 'error',
        'no-multiple-empty-lines': 'error',
        'camelcase': 'error',
        'func-style': ['error', 'expression'],
        'no-return-await': 'error',
        'no-empty-function': 'error',
        'no-extra-parens': 'error',
        'no-else-return': 'error',
        'no-restricted-syntax': 'error'
    },
};
